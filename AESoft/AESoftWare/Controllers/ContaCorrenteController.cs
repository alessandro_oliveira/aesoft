﻿using AESoftWare.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AESoftWare.Controllers
{
    public class ContaCorrenteController : Controller
    {
        List<ContaCorrente> listConta;
        ContaCorrente conta;
        public ContaCorrenteController(List<ContaCorrente> lista)
        {
            listConta = lista;
        }
        // GET: ContaCorrenteController
        public ActionResult Index()
        {
            return View();
        }
        // GET: ContaCorrenteController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
        // GET: ContaCorrenteController/Create
        public IActionResult ContaCorrenteCreate()
        {
            return View();
        }
        public ActionResult ConsultarDetalhes(int numeroConta)
        {
            conta = listConta.Find(x => x.NumeroConta == numeroConta);
            return View(conta);
        }
        public ActionResult RealizarSaque(ContaCorrente con)
        {
            conta = listConta.Find(x => x.NumeroConta == con.NumeroConta);
            return View(new ContarCorrenteValorModel() { Conta = conta, valor = 0 });
        }

        [HttpPost]
        public IActionResult Edit(ContarCorrenteValorModel contaV)
        {
            conta = listConta.Find(x => x.NumeroConta == contaV.Conta.NumeroConta);

            var realizado = conta.RealizarSaque(contaV.Conta.Valor);
            if (realizado)
                return RedirectToAction("RealizarSaque", conta);
            return RedirectToAction("RealizarSaque", conta);
        }
        public ActionResult RealizarDeposito(ContaCorrente con)
        {
            conta = listConta.Find(x => x.NumeroConta == con.NumeroConta);
            return View(new ContarCorrenteValorModel() { Conta = conta, valor = 0 });
        }
        public IActionResult EditDeposito(ContarCorrenteValorModel contaV)
        {
            conta = listConta.Find(x => x.NumeroConta == contaV.Conta.NumeroConta);

            conta.RealizarDeposito(contaV.Conta.Valor);
            return RedirectToAction("RealizarDeposito", conta);
        }
        // POST: ContaCorrenteController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ContaCorrente conta)
        {
            try
            {
                listConta.Add(conta);
                return RedirectToAction(nameof(ContaCorrentes));
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            listConta.RemoveAll(x => x.NumeroConta == id);

            return View(listConta);
        }

        public IActionResult ContaCorrentes()
        {

            return View(listConta);
        }

        // POST: ContaCorrenteController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        //public bool RealizarSaque(ContaCorrente conta)
        //{
        //    if (conta. > Saldo)
        //        return false;

        //    Saldo -= Valor;
        //    return true;
        //}
    }
}
