﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AESoftWare.Models
{
    public class ContarCorrenteValorModel
    {
        public ContaCorrente Conta { get; set; }
        public double valor { get; set; }
    }
}
