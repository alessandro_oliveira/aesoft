﻿namespace AESoftWare.Models
{
    public class ContaCorrente
    {
        public int NumeroConta{ get; set; }
        public double Saldo{ get; set; }
        public string Titular { get; set; }
        public double Valor { get; set; }
        public bool RealizarSaque(double Valor)
        {
            if (Valor > Saldo)
                return false;

            Saldo -= Valor;
            return true;
        }
        public void RealizarDeposito(double Valor)
        {
            Saldo += Valor;
        }
    }
}
